
let number = prompt("Give me a number");
console.log("The number you provided is " + number + ".");

for(let count = number; count >= 0; count--){
    if(count <= 50){
        console.log("The current value is " + count + ". Terminating the loop.");
        break;
    }

    if(count % 10 === 0){
        console.log("The number is divisible by 10. Skipping the number");
        continue;
    }

    if(count % 5 === 0) {
        console.log(count);
    }
}
/*=============================================================*/

let jargon = "supercalifragilisticexpialidocious";
console.log(jargon);

let consonant = "";

for( let i=0; i < jargon.length; i++){
	if(
		jargon[i].toLowerCase() == "a" ||
		jargon[i].toLowerCase() == "e" ||
		jargon[i].toLowerCase() == "i" ||
		jargon[i].toLowerCase() == "o" ||
		jargon[i].toLowerCase() == "u" 
	  ){
			continue;
	}
		else{
			consonant += jargon[i];
		}
}

console.log(consonant);
